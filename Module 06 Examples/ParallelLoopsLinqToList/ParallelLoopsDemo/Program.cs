﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelLoopsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Action(() => Console.WriteLine($"First {Task.CurrentId}"));
            var b = new Action(() => Console.WriteLine($"Second {Task.CurrentId}"));
            var c = new Action(() => Console.WriteLine($"Third {Task.CurrentId}"));
            Console.WriteLine("Parallel.Invoke");
            Parallel.Invoke(a, b, c);
            // these are blocking operations; wait on all tasks
            //класс Parallel применяется, чтобы распараллелить действия в циклах, 
            //т.е.все итерации будут выполняться не последовательно, а параллельно.
            //Примеры для статических методов For() и ForEach()
            Parallel.For(1, 11, x =>
            {
                Console.Write($"{x * x}\t");
            });
        }
    }
}
