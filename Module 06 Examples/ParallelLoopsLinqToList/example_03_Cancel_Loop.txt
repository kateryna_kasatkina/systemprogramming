static void Main(string[] args)
        {
            try
            {
                //DemoResult();
            }
            catch (OperationCanceledException) { }
            catch (AggregateException ae)
            {
                ae.Handle(e =>
                {
                    Console.WriteLine(e.Message);
                    return true;
                });
            }
        }