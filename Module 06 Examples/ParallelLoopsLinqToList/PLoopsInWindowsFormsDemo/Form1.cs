﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLoopsInWindowsFormsDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var a = new Action(() =>
              {
                  Form1 myForm = this;

                  myForm.Invoke(new EventHandler(delegate
                  {
                      textBox1.Text += $"First{Task.CurrentId}";
                  }));
              });


            var b = new Action(() => this.Invoke(new EventHandler(delegate { textBox1.Text += $"Second{Task.CurrentId}"; })));

            var c = new Action(() => this.Invoke(new EventHandler(delegate { textBox1.Text += $"Third{Task.CurrentId}"; })));

            Parallel.Invoke(a, b, c);
        }
       

    }
}


