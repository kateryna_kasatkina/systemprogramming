﻿using System;
using System.Threading.Tasks;

namespace example3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Demo1();
                //DemoResult();
            }
            catch (OperationCanceledException) { }
            catch (AggregateException ae)
            {
                ae.Handle(e =>
                {
                    Console.WriteLine(e.Message);
                    return true;
                });
            }
        }

        private static void Demo1()
        {
            ParallelLoopResult result = Parallel.For(0, 20,
            (int x, ParallelLoopState state) =>
            {
                Console.WriteLine($"{x}[{Task.CurrentId}]\t");
                if (x == 10)
                {
                    //throw new Exception(); //execution stops on exception
                    //state.Stop(); //stop execution as soon as possible
                    state.Break();//request that loop stop execution of iterations
                }
                if (state.IsExceptional)
                    Console.WriteLine($"EX[{Task.CurrentId}]\t");
            });
        }
    }
}
