﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProgressBarTest
{
    public partial class ProgressForm : Form
    {
        MyProgressBar pb;
        public ProgressForm()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ControlBox = false;
        }
        public ProgressForm(MyProgressBar pb)
        {
            InitializeComponent();
            this.pb = pb;
        }
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void newValues(int totalEvents, int eventsSoFar, string caption)
        {
            progressBar.Maximum = totalEvents;
            progressBar.Value = eventsSoFar;
            //lblCaption.Text = caption;
        }
        public void newValues(int totalEvents, int eventsSoFar)
        {
            progressBar.Maximum = totalEvents;
            progressBar.Value = eventsSoFar;
        }
        private void ProgressForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            pb.state = ReportState.Canceled;
        }
    }
}
