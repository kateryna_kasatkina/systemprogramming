﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DelegateThreadStart
{
    class Program
    {
        public static void Count()
        {
            for (int i =1  ; i < 9; i++)
            {
                Console.WriteLine("Second stream:");
                Console.WriteLine(i*i);
                Thread.Sleep(400);
            }
            Console.ReadLine();
        }
        static void Main(string[] args)
        {
            //В конструктор передается делегат ThreadStart, который в качестве параметра принимает метод Count. 
            Thread myThread = new Thread(new ThreadStart(Count));

            myThread.Start();//запускаем поток

            for (int i = 1; i <9; i++)
            {
                Console.WriteLine("Main stream:");
                Console.WriteLine(i*i);
                Thread.Sleep(300);
            }
                
        }
    }
}
