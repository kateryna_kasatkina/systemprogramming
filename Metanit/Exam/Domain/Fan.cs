﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
 
    public class Fan
    {
        [Key]
        public int Fan_Id { get; set; }

        public bool IsTicket { get; set; }

        public int Sector { get; set; }

        public int Place { get; set; }

        public string Name { get; set; }

    }
}
