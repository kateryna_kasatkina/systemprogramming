﻿using System.Net;
using System.Threading.Tasks;
using System.Windows;

namespace AsyncDownloadNewStyle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        async Task DoDownload()
        {

            var req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
            req.Method = "GET";
            var resp = (HttpWebResponse)await req.GetResponseAsync();

            dataTextBox.Text += resp.Headers.ToString();
            dataTextBox.Text += "Async download completed";
        }
        private void getButton_Click(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text += "Staring async download\n";
            DoDownload();
            dataTextBox.Text += "Async download started\n";
        }

        
    }
}
