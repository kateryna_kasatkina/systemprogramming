﻿using System.Net;
using System.Windows;

namespace AsyncDownloadGUIProblem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void getButton_Click(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text += "Beginning download\n";
            var req = (HttpWebRequest)WebRequest.Create("http://www.google.com");
            req.Method = "GET";
            req.BeginGetResponse(
                asyncResult =>
                {
                    var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
                    string headersText = resp.Headers.ToString();
                    dataTextBox.Text += headersText;

                },
                null);
            dataTextBox.Text += "Download started async\n";
        }

        #region GUI Update WinForm Style
        /*private void GetButtonClick(object sender, RoutedEventArgs e)
{
dataTextBox.Text += "Beginning download\n";
var sync = SynchronizationContext.Current;
var req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
req.Method = "GET";
req.BeginGetResponse(
    asyncResult =>
    {
        var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
        string headersText = resp.Headers.ToString();
        sync.Post(
            state => dataTextBox.Text += headersText,
            null);
    },
    null);
dataTextBox.Text += "Download started async\n";
}*/
        #endregion

        #region
        /*       GUI Update WinForm Style
                    /*
                    MainWindow myForm = this;
                    myForm.Invoke(new EventHandler(delegate
                    {
                        dataTextBox.Text += headersText;
                    }));
                    */
        #endregion
    }
}
