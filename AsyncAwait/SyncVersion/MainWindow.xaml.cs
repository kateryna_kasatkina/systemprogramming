﻿using System.Net;
using System.Windows;

namespace SyncVersion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        
        private void getButton_Click(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text = "Starting sync download\n";

            var req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com/");
            req.Timeout = 2000;
            req.Method = "GET";

            try
            {
                var resp = (HttpWebResponse)req.GetResponse();
                dataTextBox.Text += "Sync completed\n";
                string headersText = resp.Headers.ToString();
                dataTextBox.Text += headersText;
            }
            catch (WebException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
