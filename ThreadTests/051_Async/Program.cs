﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _051_Async
{
    class Program
    {
        static void Main()
        {
            var myDelegate = new Func<int, int, int>(Add);
            // Так как класс делегата сообщается с методами, которые принимают два целочисленных параметра, метод BeginInvoke также
            // начинает принимать два дополнительных параметра, кроме двух последних постоянных аргументов.
            IAsyncResult asyncResult = myDelegate.BeginInvoke(1, 2, // - параметры для функции Add
                                                               null,
                                                               null);

            // Ожидание завершения асинхронной операции и получение результата работы метода.
            // .Invoke - вызов в том же потоке, аналг ()
            // .BeginInvoke - асинхронный вызов. Вариант 1 - два параметра: первый - делегат обратного вызова - CallBack (null)
            //                                   Вариант 2 - параметр для CallBack (null)
            int result = myDelegate.EndInvoke(asyncResult);
            // .EndInvoke - используется только для окончания BeginInvoke (аналог Thread.Join, Task.Wait)

            Console.WriteLine("Результат = " + result);

            // Delay.
            Console.ReadKey();
        }

        // Метод для выполнения в отдельном потоке.
        static int Add(int a, int b)
        {
            Thread.Sleep(2000);
            return a + b;
        }
    }
}
