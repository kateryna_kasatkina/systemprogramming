﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _06_Thread
{
    class Program
    {
        #region Объяснение
        //Каждый поток работает в Домене приложений.

        //    Каждый поток принадлежит какому-то Домену приложений.
        //    Принадлежность может изменяться.
        //    Поток может общаться с несколькими доменнами приложения.
        #endregion
        // Общая переменная счетчик.
        //   [ThreadStatic] //TODO Снять комментарий - позволяет выделить один домен для всех потоков
        public static int counter;

        // Рекурсивный запуск потоков.
        public static void Method()
        {
            if (counter < 100)
            {
                counter++; // Увеличение счетчика вызваных методов.
                Console.WriteLine(counter + " - СТАРТ --- " + Thread.CurrentThread.GetHashCode());
                var thread = new Thread(Method);
                thread.Start();
                thread.Join(); // Закомментировать.               
            }

            Console.WriteLine("Поток {0} завершился.", Thread.CurrentThread.GetHashCode());
        }

        static void Main()
        {
            // Запуск вторичного потока.
            var thread = new Thread(Method);
            thread.Start();
            thread.Join();

            Console.WriteLine("Основной поток завершил работу...");

            // Задержка.
            Console.WriteLine("нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
