﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _07_TPL
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Основной поток запущен.");

            // Использование лямбда-оператора для определения задачи.
            Task task = Task.Factory.StartNew(new Action(() =>
            {
                Console.WriteLine("Задача запущена.");
                for (int count = 0; count < 10; count++)
                {
                    Thread.Sleep(500);
                    Console.WriteLine("В лямбда-операторе, счетчик равен: " + count);
                }
                Console.WriteLine("Задача завершена.");
            }));

            // Ожидание завершения задачи.
            task.Wait();

            // Освобождение задачи. 
            task.Dispose(); //Для сборщика мусора, в котором анонимный делигат лямбда-диспрешн был зарегистрирован

            Console.WriteLine("Основной поток завершен.");

            // Задержка.
            Console.WriteLine("нажмите любую клавишу...");
            Console.ReadKey();
        }
    }

}
