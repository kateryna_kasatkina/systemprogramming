using System;
using Microsoft.Win32;

// ��������� �� �������.

      class Program
    {
        static void Main()
        {
            // ������� ��������� ������ �� ������ RegistryKey ���������� ��������� �����.
            RegistryKey myKey = Registry.LocalMachine;
            RegistryKey software = myKey.OpenSubKey("Software");
            RegistryKey microsoft = software.OpenSubKey("Microsoft");
            // software.Close();

            Console.WriteLine("{0} - ����� ���������: {1}.", microsoft.Name, microsoft.SubKeyCount);
            microsoft.Close();

            // ������� ������� �������������� ����. ���������� ����� ��������� �������� NULL.
            software = myKey.OpenSubKey("TestName");

            // � ����� try ����������� ������� ��������� � ����������, �������� ������� �� ���������.
            try
            {
                Console.WriteLine("������� ����: {0}.", software.Name);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.GetType());
            }

            // �������� �� ������.
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }