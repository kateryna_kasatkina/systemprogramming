﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scripting; // Interop.Scripting
using Ionic.Zip;

namespace HowToRarUse
{
    class Program
    {
        static void Main(string[] args)
        {
            //FileSystemObject fso = new FileSystemObject();
            /*System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            si.FileName = @"D:\MyData\TestRar\Rar.exe";
            si.Arguments = " a " + @"D:\MyData\TestRar" + "\\" + "test.rar" + " " + @"D:\MyData\TestRar\Rar.exe";
            // " a " - параметра архиватора + Куда и по какому имени сохранить архив. Папка должна существовать  + // Что именное архивировать
            // На эту папку должны быть права Владельца
            si.UseShellExecute = false;
            si.CreateNoWindow = true;
            System.Diagnostics.Process pr = System.Diagnostics.Process.Start(si);
            pr.WaitForExit();*/
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.StatusMessageTextWriter = System.Console.Out;
                    zip.AddDirectory(@"D:\MyData\TestRar"); // Что именно архивирует
                    zip.Save(@"D:\MyData\TestRar\Rar.exe" + ".zip"); // По какому имени сохранить архив
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
