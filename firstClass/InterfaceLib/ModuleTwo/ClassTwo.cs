﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleTwo
{
    public class ClassTwo : IAssemblyExample
    {
        public string SomeMethod(int n)
        {
            return $"ModuleTwo: {n}";
        }
    }
}
